# REFACTORING

At the moment the whole VPN is fully vulnerable once an attacher owns one of the nodes. This is due to DHCP service that can be poisoned and propably Arp spoofing as long as Tinc is configured in Bridge Mode. DNS spoofing is as possible too. Current setup works but we want something more.

- I suggest following setup
    - Remove completely DHCP and use static IPs in TINC configuration
    - Set TinC in router mode using [proxy-arp][9]
    - Setup tahoe-lafs see **./tahoe-lafs/README.md**
    - SSH user for eny node **./tinc-ssh/README.md**
    - Shared [Keepass][2] database 
    - Set services on Public nodes for [bypassing restricted networks][4]
    - Internal Certificate Authority SSL
    - Offer only socket proxy ( [glider][12], [3proxy][1] ) to reach internet
    - Setup [fwknok][5], [sslh][11] or other [obfuscator][6]


### Network

On Tinc Network we have 3 type of nodes
    1. Public nodes
        - used by others nodes to access the network 
    2. Router nodes
        - bridge a physical or virtual link to allow user connected to that link to access the network 
    3. Road warriors
        - single hosts on the road rerouting their network through TinC
    3a. Tor linked ?
        - open a door to tor using socat ?

### 1. Public nodes

- Public nodes are publicly accessible for the rest of nodes to connect to the network
- Public nodes will be at least two. The more the best
- Each Node will run
    - Tinc - provide network connectivity
    - tor/cjdns (one node gives access to tor using fast [tor-router][3] and the other one to cjdns)
    - Tahoe-Lafs advertiser/server/client - see  **./tahoe-lafs/README.md**
    - Bind9 DNS server using shared volume on Tahoe-Lafs
    - Socks/Http Proxy (using [3proxy][1] ) with chain of tor and random proxies
    - 

### 2. Router node - Stable 

Services 
    - TinC bridge to physical/virtual network
    - Tahoe-Lafs Server
    - Gitea
    - Docker Registry
    - Docker remote ports with authentication and SSL
    - Pontainer ?
    - Simple Chat server ? IRC ?    
    - Can run dhcp ?
    - [IRC][8] / [Chat][7] server ?

### Notes

- Other nice ideas that propably not gonna happen any time soon
    - Setup Tinc to use Routing instead of Switch Mode ? That would be the last change we want to be able to bridge physical/virtual networks to cjdns and others.
    - What about a gateway broker. A combination of babel routing and socat tunnels ? and ip weight routing to distribute load
    - use DNSSEC 
    - Add public DNS server
    - Add simple SMTP/EMAIL server


[1]: https://github.com/QAutomatron/docker-3proxy
[2]: https://keepass.info/help/base/multiuser.html
[3]: https://github.com/znetstar/tor-router
[4]: http://seclist.us/xfltreat-tunnelling-framework.html
[5]: http://www.cipherdyne.org/fwknop/
[6]: https://github.com/OperatorFoundation/shapeshifter-dispatcher
[7]: https://github.com/mattermost/mattermost-server
[8]: https://github.com/oragono/oragono 
[9]: https://www.tinc-vpn.org/examples/proxy-arp/
[11]: https://www.rutschle.net/tech/sslh/README.html
[12]: https://github.com/nadoo/glider