#!/bin/sh

# start.sh

if [ ! -f /usr/bin/pipework ];
then 
    echo ' ! Pipework not Found - we will attempt to install it'
    curl -L -o /pipework https://raw.github.com/jpetazzo/pipework/master/pipework &&\
        chmod +x /pipework &&\
        sudo mv /pipework /usr/bin
fi

docker-compose down
docker-compose build
docker-compose up -d
pipework warrior bind_master 10.66.6.101/16
pipework warrior bind_slave  10.66.6.102/16
