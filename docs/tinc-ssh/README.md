# SSH Setup for TinC network

Following setup creates configuration for SSH server running on port 2222 using TinC RSA keys to authenticate. We create a TinC user on every node and create a python script to parse public keys of all TinC hosts. In that way every node has unprivilidged ssh access on every other node as warrior user. Itention is to use sftp only and disallow shell access.  

*tinc-ssh.yml* ansible playbook will do the following setup 

1. Create a warrior user
```
useradd -m -s /bin/bash warrior
echo "warrior:$(openssl rand -hex 64)" | chpasswd warrior
```
2. Setup dynamic script on ssh to authenticate users from TINC rsa key.
- `mkdir -p /etc/tinc/warrior/ssh`
- Copy *tinc-auth.py* under */etc/tinc/warrior/ssh/tinc-auth.py*
- `cat /etc/tinc/warrior/ssh/config`
- `chown -R root:root /etc/tinc/warrior/{ssh,hosts}`
```
PasswordAuthentication no
PidFile /etc/tinc/warrior/ssh/pid
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_dsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
AddressFamily inet
Port 2222

AllowUsers warrior
Match User warrior
  Banner /etc/banner
  AuthorizedKeysCommand     /etc/tinc/warrior/ssh/tinc-auth.py
  AuthorizedKeysCommandUser root
  # ChrootDirectory /srv/%u
  ForceCommand internal-sftp
  AllowTcpForwarding no
  X11Forwarding no
```
- Spawn dynamic ssh server on tinc-up script and kill it on disconnection.
    - Add following line to start ssh server under */etc/tinc/warrior/tinc-up* `/usr/bin/sshd -f /etc/tinc/warrior/ssh/config`
    - Add following line to stop ssh server under */etc/tinc/warrior/tinc-down* `pkill --pidfile /etc/tinc/warrior/ssh/pid`

- Other ideas
    - Use sshfs to share a folder
    - Allow port forwarding ? hmmm possibly dangerous.
    - Allow shell access ? hmmm possibly disaster.
