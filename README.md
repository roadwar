# Deploy ( Development )

```
vagrant up
ansible-playbook network.yml
```

# REPOS

- Distribute repository to 2 or more remote git servers 
    - find an obscure but stable git repo
        - https://savannah.gnu.org/
        - https://www.tuxfamily.org/
        - http://repo.or.cz/
        - https://rocketgit.com/
        - gitlab.s-3.tech
        - gitlab.com ? propably not

- Simplify deployment using ansible-pull
- Propably git hooks ?

# Developer

- Register User: http://repo.or.cz/reguser.cgi
- Ask admin to add user: https://repo.or.cz/editproj.cgi
- The push URL(s) for the project:
	- ssh://repo.or.cz/roadwar.git
	- https://repo.or.cz/roadwar.git

SSH configuration *~/.ssh/config*

```
Host repo.or.cz
  User <username>
  Hostname repo.or.cz 
  IdentityFile <ssh key>
```

# User 

- The read-only URL(s) for the project:
	- git://repo.or.cz/roadwar.git
	- http://repo.or.cz/roadwar.git

-----------------------------------------------------------------------------


Under development - currently broken

```
ansible-galaxy install -r requirements.yml
vagrant up
ansible-playbook -i inventory/vagrant.yml refactoring/vpn.yml
ansible-playbook -i inventory/vagrant.yml refactoring/v2ray.yml
```